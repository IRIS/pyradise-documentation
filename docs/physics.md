# Physics in PyRADISE

$\texttt{PyRADISE}$ simulates the evolution of the transverse bunch distribution $\Psi$ in high-energy hadron colliders due to noise driven diffusion. It models the diffusion self-consistently, when the diffusion depends on the bunch distribution. The evolution of the stability is evolved continuously by calculating the stability diagrams with $\texttt{PySSD}$.

The various types of diffusion that is implemented will be shortly explained here in terms of a diffusion coefficient $D$ as in the following diffusion equation
$$
\frac{\mathrm{d}\Psi}{\mathrm{d}t} = \frac{\mathrm{d}}{\mathrm{d} J}\left(JD\frac{\mathrm{d}\Psi}{\mathrm{d}J}\right)
$$

What physics is modeled is chosen by the input parameter iCoeff (see [How to setup a simulation](input.md))

| iCoeff    | Physics |
|:---------:|----|
| 1 | Intra-beam scattering (IBS)
| 2 | IBS + Feedback affected diffusion (FB)
| 3 | IBS + Wakefield driven diffusion (W-USHO)
| 4 | IBS + FB + W-USHO
| 5 | IBS + Wakefield driven diffusion (W-VLASOV)


## Intra-beam scattering (IBS)

Diffusion driven by IBS affects each particle individually.
In $\texttt{PyRADISE}$, the diffusion driven by IBS is assumed uniform, meaning that the diffusion coefficient $D$ is a constant.


## Noise
Here, noise refers to external stochastic forces that is correlated along the bunch.
The most typical type of noise is rigid noise, affecting each particle in a bunch equally.

If there is only noise and a source of decoherence (different transverse betatron tunes for different particles), the diffusion due to noise will again be uniform, meaning that the diffusion coefficient $D$ is a constant.


## Noise and a feedback system (FB)
A feedback system can limit the emittance growth due to noise.
That means that it also limits the diffusion driven by noise.
However, the diffusion is no longer uniform, but depends on the tunes of the particles.
$\texttt{PyRADISE}$ has implemented the diffusion coefficient derived in
[doi:10.1103/PhysRevAccelBeams.23.034401](https://doi.org/10.1103/PhysRevAccelBeams.23.034401).


## Noise and wakefields (W-USHO and W-VLASOV)
Wakefields are what can cause instabilities in the current version of $\texttt{PyRADISE}$.
At the beginning of the simulations, the beam eigenmodes driven by wakefields are stable, either inherently or due to Landau damping. If not, there will be no simulation as the beam is already unstable.
By exciting the beam with transverse noise, the eigenmodes act on the beam through wakefields, which produce an additional diffusion. This diffusion can change the distribution so that Landau damping is lost and the beam becomes unstable, as have been observed in the Large Hadron Collider.

$\texttt{PyRADISE}$ has implemented two models for the noise and wakefield driven diffusion:

  * W-USHO: Model the beam eigenmodes as underdamped stochastic harmonic oscillators (USHO) as described in<br>
    [doi:10.1103/PhysRevAccelBeams.23.114401](https://doi.org/10.1103/PhysRevAccelBeams.23.114401).
  * W-VLASOV: Model the beam eigenmodes with the linear Vlasov equation, as described in:  
    "Vlasov description of the beam response to noise in the presence of wakefields in high-energy synchrotrons: Beam transfer function, diffusion, and loss of Landau damping", to be submitted for publication.

In both models, the diffusion driven by an almost unstable mode is modeled by a diffusion coefficient that is peaked in tune space, meaning that it can cause a local flattening of the distribution in action space if the detuning depends on the action in the same plane as the noise and wakefields, which is often the case. The local flattening corresponds to the drilling of a hole in the stability diagram, which means that modes that initially were stable can become unstable.
