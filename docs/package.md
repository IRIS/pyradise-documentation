
The file and class structure of $\texttt{PyRADISE}$ are summarized in Fig. 1.
Most parameters and methods are defined in a SolverClass object. The SolverClass inherits from three other classes: StabilityDiagramClass, PostProcessingClass, and PlottingClass.
The SolverClass object also contains additional objects, one of the GridClass and one of the MachineClass.
The MachineClass itself contains a NoiseClass object and a LinearDetuningClass object.
There are also external methods that are used by the SolverClass object, but that can also be used without a SolverClass object.

<figure>
    <img src="../fig/classstructure.jpg"  />
    <figcaption>Fig. 1: Illustration of the PyRADISE package. Most parameters and calculations are defined in a SolverClass object. Some functions are defined externally to this object. Classes called "NameClass" are defined in files called approximately "Name.py" (some exceptions are noted indirectly below).  </figcaption>
</figure>


## The Solver object

### StabilityDiagram.py - StabilityDiagramClass
Methods related to calculating the stability diagram.
The StabilityDiagramClass is inherited by SolverClass.
This file includes external methods calculating the stability diagram tune shift by use of $\texttt{PySSD}$.

In the function calc_Stability_Diagram_OneQ(), it is hardcoded whether or not the tune shift should be calculated based on 1, 2, or 3 calls to the dispersion integral (if more calls are used, the value at ${\epsilon\rightarrow0}$ is estimated with a first- or second-order Taylor expansion).

### PostProcessing.py - PostProcessingClass
Methods related to postprocessing of the distribution as it evolves.
The PostProcessingClass is inherited by SolverClass.
The methods include, but are not limited to, calculating the action moments of the distribution ${\langle J^n\Psi\rangle}$, calculating the 1D projections of the distribution and its derivative in action space, and the projections of the distribution in position space (i.e. from action $J_x$ to position $x$, assuming a uniform azimuthal distribution).

### Plotting.py - PlottingClass
Methods related to plotting the distribution and diffusion coefficients (methods related to plotting the stability diagram are gathered in the StabilityDiagramClass).
The PlottingClass is inherited by SolverClass.

### Solver.py - SolverClass
The main calculation class, which inherits from the StabilityDiagramClass, PostProcessingClass, and PlottingClass, described above, and it contains objects of the GridClass and MachineClass (which itself contains objects of the NoiseClass and DetuningClass) described below.

The calculation object of the SolverClass is what will be carried around and stored in pickle files as the calculation goes along.

The methods are related to solving the PDE, including:
setting (and updating) the discrete times for when the distribution and stability diagram should be calculated,
interpolation of the distribution,
calculating the stability diagram and damped modes and checking the related stability,
calculating the coefficients (by calls to functions in Coefficients.py described below) and organizing them on the discrete grid,
and obviously also solving the PDE time-step by time-step within the method solve().

### Grid.py - Grid2DClass, Grid1DClass
Mostly a Namespace, containing the parameters related to the discretization of the transverse action space ${(J_x,J_y)}$.
That includes stencils needed to calculate the average or change of the distribution function ${\Psi_{i,j}}$ between cells in any direction (east, west, north, or south).

A GridClass object is contained within the SolverClass object.

There are different classes for 1D and 2D. However, the 2D class can now also handle a 1D grid.

### Machine.py - MachineClass
Mainly a Namespace to include the real physics parameters.
This includes the detuning, the feedback system gain, the noise, the average tune shift of the beams (relevant for the FB diffusion).
It also includes all the necessary details about the wakefield driven beam eigenmodes (which can be supplied by $\texttt{BimBim}$ or $\texttt{DELPHI}$).

A MachineClass object is contained within the SolverClass object.

### Noise.py - NoiseClass
A Namespace, containing parameters for the noise amplitude in the machine.

A NoiseClass object is contained within the MachineClass object.

### Detuning.py - LinearDetuningClass
Mainly a Namespace to include the transverse detuning dependence on the transverse coordinates (not chromaticity).
It also contains methods to get the transverse detuning as function of the transverse actions ${(J_x,J_y)}$.
Used to calculate the stability diagram with help of $\texttt{PySSD}$

A LinearDetuningClass object is contained within the MachineClass object.


## External functions

### Distribution.py - MyDistribution1D, MyDistribution2D
Defines classes used to interpolate the distribution defined in the discrete transverse action grid defined in Grid.py (by calls to the method calcInterpDist() in the SolverClass).

There are different classes for 1D and 2D.
There are also classes in both 1D and 2D whose name end on "y", designed to get the stability diagram in the vertical plane.
They were implemented because the original $\texttt{PySSD}$ could only calculate the stability diagram in the horizontal plane.
Hence, this simply transposes the original distribution, exchanging X and Y, compared to the classes not ending in "y".

### Coefficients.py
Functions to calculate the various diffusion coefficients described in [physics](physics.md), either as a function of the grid, the actions, or a given incoherent detuning.

This file also includes the functionality to calculate the Landau damped tune shift of a wakefield eigenmode, required to calculate the diffusion coefficient with the USHO model for the wakefield driven diffusion.

### PySSDHelper.py
Functions to calculate the discrete tunes (array) for which you want to calculate the stability diagram.

get_tune_range() is taken from $\texttt{PySSD}$, giving a linearly spaced set of tunes.

get_tune_range_focused() is an adapted version, which gives a higher density of linearly spaced tunes in a small area (equally many points inside a small area as outside it).
This is useful, as we mostly care about the stability diagram close to where a hole might be drilled due to the wakefield driven diffusion.

### L2D2_functions.py

Functions designed specifically to the concept of L2D2 (Loss of Landau Damping by Diffusion).
That includes finding the analytical latency with the USHO model for 1 mode, calculating the Landau damped tune shift, and iteratively finding the detuning required to either barely stabilize a mode or to get a chosen analytical latency for a certain noise etc.
