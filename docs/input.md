
## Setup of PyRADISE
The first step to setting up $\texttt{PyRADISE}$ is to take care of the paths.
That means changing the Namespace called "paths" in setup_PyRADISE.py, especially paths.ToPackage, which is the path to where you have stored both the folder $\texttt{PyRADISE}$ and $\texttt{PySSD}$.
You can also choose where you want to store the output files and plots.


## Input parameters
Here, all the input parameters to $\texttt{PyRADISE}$ is explained.<br>
The recommended parameter values are given in bold (they might differ from the default values).


### SolverClass parameters
| Parameter             |   Possible values     | Description       |
|-----------------------|-----------------------|-------------------|
| solve_method          | 'RK45', 'RK23', 'Radau', **'BDF'**, 'LSODA' | What method to use to solve the disctetized PDE in time (in principle an ODE per grid cell). <br>Should definitely use 'BDF'. If not, it is numerically unstable.
| flag_ICPoint          | True/**False**        | If True, set the IC distribution as the point value in the center of the cell or the average over the cell. If False, take the average value over the grid volume. Less important for finer grid resolution. <br>Get faster convergence with the grid resolution when this is set to False
| flag_UpdateCoeff      | **True**/False        | If True, recalculate the diffusion coeffiecent every time-step. This is needed to be physically correct if the diffusion coefficient depends on the evolving distribution.
| flag_UpdateCoeffAllTheWay | **True**/False    | If False (and flag_UpdateCoeff is True): Do not update coefficients the last X% of the drilling (see Solver.py for the value of X). This flag was implemented to study numerical effects as the instability was approached.
| relInstabilityThreshold | $[\mathbf{0},1)$         | (If flag_UpdateCoeff is True): Consider instability reached when relInstabilityThreshold*100% remains of the drilling. This is a non-physical possibility implemented to deal with numerical issues.
| flag_SDRemoveLoops    | True/**False**        | If True (and flag_UpdateCoeff is True): Remove loops of SD to calculate the diffusion coefficient. This is a non-physical possibility implemented to deal with numerical issues.
| grid  / G             | N/A                   | GridClass object (see below).
| machine / M           | N/A                   | MachineClass object (see below).
| iCoeff                | (int) $1,2,...,5$     | Which diffusion coefficient to be simulated. See [physics](physics.md) for more info. <br> 1: Intra-beam scattering (IBS) <br> 2: IBS + Feedback affected diffusion (FB) <br> 3: IBS + Wakefield driven diffusion (W-USHO) <br> 4: IBS + FB + W-USHO <br> 5: IBS + Wakefield driven diffusion (W-VLASOV)
| flag_IncludeDirectNoise | **True**/False      | If True, model diffusion driven directly by the noise and by the wake. If false, only wake noise. <br> True is the most physically correct option.
| tmax                  | $(0,\infty)$          | Max number of seconds to iterate the distribution for. <br>May be changed if flag_AdjustTmax is True. <br> The iteration will stop earlier if the beam becomes unstable.
| flag_AdjustTmax       | **True**/False        | If True, calculate approximately how long the latency will be to set tmax, the maximum time until for which to solve the PDE. As the latency varies over multiple order of magnitude, this should be set to True, unless you have tested a specific configuration in detail.
| ntODE                 | (int) $2,3...$        | Number of times to solve the distribution for, max number if flag_AdjustTmax is True. <br>This includes the IC at time #1.
| tsODE                 | N/A                   | Array of times for when to solve the distribution for. <br>May be changed if flag_AdjustTmax is True.
| ntPSImax              | (int) $2,3,...$       | Maximum number of times to save the distribution for. Can be set smaller than ntODE if you want to have a better resolution in time when solving the PDE, but do not want to save every step (to limit memory usage).
| interpOrderPsi        | (int) $0,\mathbf{1},2,3$ | Interpolation order of the distribution. <br>0: Nearest <br>1: Linear <br>2: Quadratic <br>3: Cubic
| debug                 | (int) $\mathbf{0},1,2,3$ | If>0: Print additional information to user. Different layers of debug information (a little with debug=1, some more with debug=2, etc).
| maxCPU                | (int) $\mathbf{1},2,...$ | Maximum number of CPUs to use for SD calculation, using the python package multiprocessing, which can produce memory overload issues. Hence, it is recommended to only use 1 cpu, especially if the numerical parameters are pushed so that the memory becomes and issue...
| flag_Print            | **True**/False        | If True, print key input parameters to the terminal.

Parameters only relevant for the W-USHO method (also relevant for calculating the analytical latency)

| USHO parameters       |   Possible values     | Description       |
|-----------------------|-----------------------|-------------------|
| flag_FindAlpha        | [**True**/False,**True**/False] | If True, find the [real,imaginary] part of taylorAlpha. <br>If False, set taylorAlpha $=1+0j$. <br>This is part of finding the Landau damped tune of the wakefield driven modes, using taylorAlpha as the coefficient of the first-order Taylor expansion from the stability diagram to the wakefield driven tune shift inside the stability diagram.
| flag_UpdateAlpha      | **True**/False        | If True (and either entry of flag_FindAlpha is True), update taylorAlpha at each time-step.
| flag_UpdateReQ        | **True**/False        | If True, update the real part of the Landau damped tune shift every time-step. Setting this to False is an unphysical ad-hoc solution to a numerical problem, as the real tune of the damped modes can shift significantly and unphysically towards the end of the simulation if the initial stability margin is large. Such a drift leads to a local flattening of the distribution at different actions, and therefore stops the process of drilling a hole in the stability diagram.


### Grid parameters
| Parameter             |   Possible values     | Description       
|-----------------------|-----------------------|-------------------
| flag_AdjustGrid       | True/**False**        | If true, shift grid so that one cell is centered on the average tune. Only for a 1D grid.
| flag_Radial           | **True**/False        | If True, solve the PDE in phase space radius coordinates (the alternative is the action). <br>The convergence with the grid resolution is faster with this set to True. It is less important for finer grid resolution.
| ND                    | (int) $1,\mathbf{2}$  | Number of transverse action dimensions, 1 or 2.
| Ncx                   | (int) $2,3,...$         | Number of horizontal grid cells.
| Ncy                   | (int) $1,2,...$         | Number of vertical grid cells. If equal to 1, ND is overwritten to 1.
| JxMax                 | $(0,\infty)$          | Max horizontal action in the PDE.
| JyMax                 | $(0,\infty)$          | Max vertical action in the PDE.
| BC_xMax               | (int) $\mathbf{0},1$  | What boundary condition (BC) to use at the max value of $J_x$. <br>0: Dirichlet (fixed value), like an aparture (if the value is set to 0) <br>1: Neumann, reflective boundary (if the value is set to 0).
| BC_xVal               | $(-\infty,\infty)$    | Value of boundary condition (BC) to use at the max value of $J_x$. <br>Should be 0 - No reasonable physics for anything else. <br>Also, not debugged recently for anything else.
| BC_yMax               | (int) $\mathbf{0},1$  | As above, but in the vertical plane.
| BC_yVal               | $(-\infty,\infty)$    | As above, but in the vertical plane.


### MachineClass parameters
| Parameter             |   Example value       | Description       
|-----------------------|-----------------------|-------------------
| detuning / Q          | N/A                   | LinearDetuningClass object (see below).
| noise / N             | N/A                   | NoiseClass object (see below).
| f_rev                 | $11245.5$ (LHC)       | Revolution frequency (relevant since noise and more are given in values per turn).
| gx                    | $0.0$                 | Horizontal feedback gain, damping time in turns is equal to 2/gx.
| gy                    | $0.0$                 | Vertical feedback gain, damping time in turns is equal to 2/gy.
| Qpx                   | $15.0$                | Horizontal linear chromaticity, $Q_x'$.
| Qpy                   | $15.0$                | Vertical linear chromaticity, $Q_y'$.
| Qs                    | 0.00191               | Synchrotron tune.
| sigma_dpp             | $10^{-4}$             | RMS momentum spread. Currently not in use, as it is found the diffusion does not depend on it to first order.
| dQxAvg                | $0$                   | Relevant for feedback affected diffusion. <br>Average horizontal tune shift from Q0x.
| dQyAvg                | $0$                   | Relevant for feedback affected diffusion. <br>Average vertical tune shift from Q0y.
| damperAlpha           | $1$                   | Relevant for feedback affected diffusion. <br>Parameter deciding whether or not there will be a drift term. Found that 1 is the most physically correct value. <br>0: includes the drift term <br>1: fully cancel the drift term
| wmode__DQx            | $[-10^{-4}+10^{-5}]$   | List of wakefield driven tune shifts of horizontal wakefield eigenmodes, $\Delta Q_m^\mathrm{coh}$.
| wmodeQ0x              | $[0.31]$               | List of tunes of horizontal wakefield eigenmodes subtracted the wakefield driven tune shifts. <br>Currently not in use.
| wmodeMom0x            | $[0.01]$               | List of zeroth-order noise-mode-moments of horizontal wakefield eigenmodes, $\eta_{m 0}$.
| wmodeMom1x            | $[0.1]$                | List of first-order noise-mode-moments of horizontal wakefield eigenmodes, $\eta_{m 1}$.
| wmodeQ0y              | $[~]$                  | As above, but in the vertical plane.
| wmode__DQy            | $[~]$                  | As above, but in the vertical plane.
| wmodeMom0y            | $[~]$                  | As above, but in the vertical plane.
| wmodeMom1y            | $[~]$                  | As above, but in the vertical plane.



### LinearDetuningClass parameters
| Parameter             |   Example value     | Description       
|-----------------------|-----------------------|-------------------
| Q0x   | $0.31$            | Horizontal fractional tune.
| Q0y   | $0.32$            | Vertical fractional tune.
| ax    | $6\times 10^{-5}$ | Horizontal normalized in-plane detuning coefficient.
| bx    |$-4\times 10^{-5}$ | Horizontal normalized cross-plane detuning coefficient.
| ay    | $6\times 10^{-5}$ | As above, but in the vertical plane.
| by    |$-4\times 10^{-5}$ | As above, but in the vertical plane.


### NoiseClass parameters
| Parameter     |   Possible values | Description       
|---------------|-------------------|-------------------
| f_rev         | $11245.5$ (LHC)   | Revolution frequency (relevant since noise is given in amplitude per turn).
| sigma_k0x     | $[0,\infty)$      | RMS amplitude of rigid noise in the horizontal plane, per turn, in units of the beam size. <br>A reasonable value for the LHC is $\sim10^{-4}$.
| sigma_k1x     | $[0,\infty)$      | RMS amplitude of headtail noise ($\propto z$) in the horizontal plane, per turn, in units of the beam size.
| sigma_ibsx    | $[0,\infty)$      | RMS amplitude of intra-beam scattering noise in the horizontal plane, per turn, in units of the beam size.
| sigma_k0y     | $[0,\infty)$      | As above, but in the vertical plane.
| sigma_k1y     | $[0,\infty)$      | As above, but in the vertical plane.
| sigma_ibsy    | $[0,\infty)$      | As above, but in the vertical plane.

### StabilityDiagramClass parameters  
| Parameter             |   Possible values     | Description       
|-----------------------|-----------------------|-------------------
| ntSDmax               | (int) $2,3,...$       | Max number of distributions $\Psi$ to calculate the stability diagram for after solving. Best if this is equal to ntPSImax.
| scales                | $[~]$                 | Relevant for the feedback affected diffusion. <br>One can compare the stability diagram for all the distributions with the given detuning parameters ${a_x,~b_x,...}$ to scaled stability diagrams with the initial stability diagram. This parameter is a list of scales for which to calculate the initial stability diagram (should contain values from 0 to a little over 1). This is to check the effective detuning strengths.
| nQ                    | (int) $2,3,...$       | Number of tunes for which to calculate the stability diagram. <br>Should be in the order of 1000 if the W-VLASOV diffusion is used.
| integrator_epsilon    | $\rightarrow 0^{+}$   | Key small parameter in calculating the dispersion integral with $\texttt{PySSD}$. <br> The convergence with this parameter is quite challenging. <br> A good suggestion is to use \|${a_x}/200$\|, but it may have to be larger if $b_x=0$ due to numerical effects.
| JMaxSD                | $(0,\infty)$          | Max action to integrate over with $\texttt{PySSD}$. Must be maximum the maximum action set in the grid parameters.
| nStep                 | (int) $2,3,...$       | Number of points to discretize transverse action space in per dimension from 0 to JMaxSD. <br>Default in $\texttt{PySSD}$ is 2000. This can be reduced to run simulations faster, but should be at approximately this value.
| interpOrderSD         | (int) 0,**1**,2,3     | Interpolation order of the stability diagram, both for calculating the diffusion coefficient and checking the stability with the W-VLASOV method, and for plotting the stability diagrams. <br>0: 'nearest' <br>1: 'linear' <br>2: 'quadratic' <br>3: 'cubic'
| widthRatioSD          | $(0,1)$               | nQ/2 of the free tunes for which the stability diagram is calculated are in a smaller interval within the total range around the most unstable mode. This is to get a higher resolution in the key area. <br> The ratio of the smaller interval width to the total width is  widthRatioSD. <br>  Reasonable values are $(0.05,0.10)$. <br> If outside the interval $(0,1)$, the code will not try to be clever, but simply use a simple linearly spaced array of tunes.
| flag_center_tune_is_LDQ| True/**False**       | If True, find and use the Landau damped tune shift of the least stable mode as the center of the increased resolution. <br> If False, use the original wakefield driven tune shift of the least stable mode as the center tune. <br> Using the Landau damped tune is slightly more accurate but also more time consuming (and can in principle lead to problems if the hole is deep as for the USHO method in general). Using the original tune shift (False) is often sufficient.
| flag_Print            | **True**/False        | If True, print key input parameters to the terminal.


### PostProcessingClass parameters  
| Parameter             |   Possible values     | Description       
|-----------------------|-----------------------|-------------------
| flag_ProjX            |  True/**False**     | If True, calculate the projection on x and y axis (time consuming and rarely interesting).
| xMax_ProjX            | (0,$\infty$)          | Maximum position amplitude until which the projection goes.
| flag_Print            | **True**/False        | If True, print key input parameters to the terminal.

### PlottingClass parameters  
| Parameter             |   Possible values     | Description       
|-----------------------|-----------------------|-------------------
| time_scale            | $0,\mathbf{1},2,3,\mathrm{`auto'}$    | To choose the prefactor of the plotting. <br>0: ms <br>1: s <br>2: min <br>3: h <br>'auto': Choose dependent on tmax (which may be changed if flag_AdjustTmax is True).
| plot_rmax             | $(0,\infty)$          | Maximum radius for which to plot until. Remember that $2J=r^2$.
| flag_Fill             | True/**False**        | If True, color for each time-step the change of the distribution from the previous distribution.
| flag_Print            | **True**/False        | If True, print key input parameters to the terminal.
