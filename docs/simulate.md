
This guide assumes that you are using the [helper files](helperfiles.md).

To run the simulation, you can simply execute the script calc_PyRADISE.py
```
$ python3 calc_PyRADISE.py
```
which will automatically take the input parameters from setup_PyRADISE.py. A more thorough explanation follows below.

<!-- ## Staged calculation (as in v.2.0.0) -->

## Staged calculation in PyRADISE
The calculation in $\texttt{PyRADISE}$ is staged. After each stage, the SolverClass object can be stored.
Whether or not you want to save the SolverClass object after the various stages of the simulation can be decided by toggling the parameters "flag_SavePkl_S*" in calc_PyRADISE.py.

Assume in the following that we call the study "studyname".

### Setup: Input parameters
To setup a simulation, the parameters can be set in setup_PyRADISE.py, as described in [how to setup a simulation](input.md).

The key parameters that increase both the memory consumption and wall time of the calculation (and should be set small if you are testing) are:  

  - Ncx: Number of cells in the horizontal action dimension.
  - Ncy: Number of cells in the vertical action dimension.
  - ntODE: Number of time-steps for which to solve the PDE.
  - nQ: Number of tunes in each stability diagram.
  - nStep: Number of points to discretize transverse action space in per dimension from 0 to JMaxSD when numerically calculating the dispersion integral with $\texttt{PySSD}$.  



### Stage 0: Initialization
Initialize the SolverClass object based on the setup with the function calc_init() in calc_PyRADISE.py.
There are two ways to load the setup:  

Either create a setup file "studyname_Setup.pkl" that will be stored in the directory paths.ToStorage, i.e. by calling setup_PyRADISE.py. You can then load this setup file to initialize $\texttt{PyRADISE}$ with calc_init() by calling calc_PyRADISE.py
```
$ python3 setup_PyRADISE.py
$ python3 calc_PyRADISE.py studyname_Setup
```

Alternatively, you can load the setup Namespace directly from setup_PyRADISE.py by calling
```
$ python3 calc_PyRADISE.py
```
If flag_SavePkl_Setup in calc_PyRADISE.py is True, this will store a pickle file "studyname_Setup.pkl".


If flag_SavePkl_S0 in calc_PyRADISE.py is True, the $\texttt{PyRADISE}$ object will be stored in a pickle file named "studyname_S0.pkl".
This file will then include the initial distribution, diffusion coefficient, and relevant initial stability diagrams. This can be useful for debugging and for checking that you are about to simulate what you want to.

To check the initialization, use the function testPlots_S0() in plot_PyRADISE.py by calling
```
$ python3 plot_PyRADISE.py studyname_S0
```

### Stage 1: Solve the distribution evolution
Solve the PDE with the function calc_PSI() in calc_PyRADISE.py.
If iCoeff is 5, this stage will also achieve the calculation of the stability diagram.
If iCoeff is 3, 4 or 5, the evolution of the stability margin will be calculated during this stage.
At the end of this stage, some distributions may be deleted. This is done to reduce the memory and disk consumption. The maximum number of time-steps is set by the parameter "ntODE". The maximum number of distributions is set by the parameter "ntPSImax".

If flag_SavePkl_S1 in calc_PyRADISE.py is True, the $\texttt{PyRADISE}$ object will be stored in a pickle file named "studyname_S1.pkl".
This file will include the evolution of the distribution (and potentially the stability diagram).

To check the distribution evolution, use the function testPlots_S1() in plot_PyRADISE.py by calling
```
$ python3 plot_PyRADISE.py studyname_S1
```

### Stage 2: Calculate the stability diagrams
Calculate the stability diagram and various related quantities at all the discrete times for which the distribution was stored in the previous stage, by calling the function calc_SD() in calc_PyRADISE.py.
If the stability diagram was calculated during Stage 1 (if iCoeff is 5), it will not be recalculated.

If flag_SavePkl_S2 in calc_PyRADISE.py is True, the $\texttt{PyRADISE}$ object will be stored in a pickle file named "studyname_S2.pkl".

To check the stability evolution, use the function testPlots_S2() in plot_PyRADISE.py by calling
```
$ python3 plot_PyRADISE.py studyname_S2
```

### Stage 3: Only save key results
If flag_SavePkl_S3 in calc_PyRADISE.py is True, the key results will be stored in a pickle file named "studyname_S3.pkl".
For now, that includes quantities related to the latency, but it can be changed by modifying the list in the function dump_results() in calc_PyRADISE.py.
This file requires much less memory than "studyname_S2.pkl".

To check the key results, use the function testPlots_S3() in plot_PyRADISE.py by calling
```
$ python3 plot_PyRADISE.py studyname_S3
```
