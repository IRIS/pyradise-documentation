
Several helper files have been created outside the $\texttt{PyRADISE}$ package, but still inside the [GitLab repository](https://gitlab.cern.ch/IRIS/pyradise), to simplify the usage of the simulation tool. This are not necessary, but helpful.

## setup_PyRADISE.py
Set all the input parameters discussed in [how to setup a simulation](input.md), and potentially store them in an input file.

## calc_PyRADISE.py
Perform the actual calculation to get results.

## plot_PyRADISE.py
Generate several test plots of the results.

## _plotconfig.py
Configure how the plots should look like.
