
$\texttt{PyRADISE}$ is a PDE solver connected to $\texttt{PySSD}$, a stability diagram solver.
The acronym "$\texttt{PyRADISE}$" stands for "<u>Py</u>thon <u>RA</u>dial <u>DI</u>ffusion and <u>S</u>tability <u>E</u>volution".
The code simulates the evolution of the transverse bunch distribution and high-energy hadron colliders due to noise driven diffusion. It models the diffusion self-consistently, when the diffusion depends on the bunch distribution. The evolution of the stability is evolved continuously by calculating the stability diagram.

This documentation explains shortly the [physics](physics.md) $\texttt{PyRADISE}$ can simulate and how the PDE is solved with a [Finite Volume Method](FVM.md).

[The PyRADISE package](package.md) structure is explained with an emphasis on the class hierarchy.

[Helper files](helperfiles.md) have been made to guide you on [how to setup a simulation](input.md) and [how to run and understand a simulation](simulate.md).

## Web resources
  * [GitLab repository](https://gitlab.cern.ch/IRIS/pyradise), including 1 testcase.
  * [First paper explaining the FVM solver (2019)](https://doi.org/10.1103/PhysRevAccelBeams.23.034401)
  * [First paper on the wakefield driven diffusion and corresponding loss of Landau damping (2020)](https://doi.org/10.1103/PhysRevAccelBeams.23.114401)


## Technical information
  * **Programming Languages used for implementation:**
      * Python
  * **Operating systems:**
      * Tested exclusivey on Linux (Ubuntu 16.04 and CENTOS7)
  * **Other prerequisites:**-->
      * Python packages: numpy, scipy, copy, matplotlib, sys, os, time, multiprocessing
      * $\texttt{PySSD}$


## Other informations
   * <b>Developed by : </b>CERN
   * <b>License : </b>CERN Copyright
   * **Contact persons** : [Xavier Buffat](http://phonebook.cern.ch/phonebook/#personDetails/?id=699946 "CERN phone book"),
                           [Sondre Vik Furuseth](mailto:s.v.furuseth@gmail.com "email address")
   * **Author of this documentation** :
                           [Sondre Vik Furuseth](mailto:s.v.furuseth@gmail.com "email address")
