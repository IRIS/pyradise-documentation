# Finite Volume Method

$\texttt{PyRADISE}$ solves the diffusion equation discussed in [physics](physics.md) with a finite volume method (FVM).
What is presented here is taken from appendix D of [this doctoral thesis](http://dx.doi.org/10.5075/epfl-thesis-9330). The use of the FVM is discussed in greater detail in the thesis, including the derivations of the fluxes across the borders.


## Discretization in space

The diffusion equation is solved in a 2D transverse action or phase space radius space, which is discretized as in Fig. 1 $(\texttt{PyRADISE}$ can also solve the diffusion in 1D horizontal action space, but that is physically irrelevant).
A uniform ${N_x \times N_y}$ cell grid covers the independent variables, either in the form of $J$ or $r$, from 0 to their respective maximal values. Each cell $C_{i,j}$ in the grid has a center ${c_{i,j}=\left(\left(i+\tfrac{1}{2}\right)\cdot h_x, \left(j+\tfrac{1}{2}\right)\cdot h_y\right)}$ and four edge points $e_{i,j},~w_{i,j},~n_{i,j},~s_{i,j}$, which are half a cell width or height away from $c_{i,j}$ in the respective compass directions. It follows that ${e_{i,j}=w_{i+1,j}}$, etc.

<figure>
    <img src="../fig/FVM_PyRADISE_hxhy.png"  />
    <figcaption>Fig. 1: Illustration of the FVM grid in PyRADISE.  </figcaption>
</figure>

The density in a certain cell, $\Psi_{i,j}$, is considered independent of $(J_x,J_y)$ within that cell.
The evolution of the density in a cell depends on the diffusion coefficients at the four cell edges, as well as the distribution function itself in the neighboring cells. The total distribution density is preserved by modeling the changes of the distribution as a flux between neighboring cells - what is lost in one cell is gained in another. That is an inherent property of the FVM, which is why this discretization scheme was chosen for $\texttt{PyRADISE}$.


The boundary conditions (BC), i.e. the fluxes through the borders of the global volume, must be treated carefully.
At ${J=0}$, physics dictates that there must be a reflective BC, i.e. with no flux through it.
At the boundary at ${J=J_\mathrm{max}}$, one can argue between using a homogeneous or inhomogeneous Neumann or Dirichlet BC.
It has been decided most correct to model it as an absorbing boundary (homogeneous Dirichlet), representing an aperture in a real machine. This is achieved by introducing virtual ghost cells with zero density outside the boundary.


## Discretization in time

The time integration is performed by $\texttt{scipy.integrate.solve_ivp}$, using a method based on a backward differentiation formula and a sparse Jacobian.
Using an implicit scheme ensures the numerical stability of the integration.
In the cases when the diffusion and drift coefficients depend on the distribution, the coefficients are calculated based on the distribution at time $t_a$,
whereupon the distribution evolution from $t_a$ to $t_b$ is evaluated,
before the coefficients are recalculated ahead of the next time-step.
This is acceptable due to the small change of the distribution within each short time-step.
The output is an array ${\Psi_{i,j,k}}$ that contains the density in all cells $C_{i,j}$ at the desired discrete times $t_k$.
The stability is considered by solving the stability diagram for the distribution at the discrete times $t_k$.
